﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{

    [Tooltip("In m/s")][SerializeField] float speed = 40f;

    [SerializeField] float positionPitchFactor = -0.8f;
    [SerializeField] float controllPitchFactor = -10f;
    [SerializeField] float positionYawFactor = 0.8f;
    [SerializeField] float controllRowFactor = -25f;

    float xThrow, yThrow, xOffset, yOffset;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frames
    void Update() {
        ProcessTranslation();
        ProcessRotation();
    }

    private void ProcessRotation() {
        float pitch = transform.localPosition.y * positionPitchFactor + yThrow * controllPitchFactor;
        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controllRowFactor;
        transform.localRotation = Quaternion.Euler(pitch,yaw,roll);
    }

    private void ProcessTranslation() {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        xOffset = xThrow * speed * Time.deltaTime;
        float rawNewXPos = transform.localPosition.x + xOffset;
        float clampedX = Mathf.Clamp(rawNewXPos, -40f, 40f);

        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        yOffset = yThrow * speed * Time.deltaTime;
        float rawNewYPos = transform.localPosition.y + yOffset;
        float clampedY = Mathf.Clamp(rawNewYPos, -18f, 18f);

        transform.localPosition = new Vector3(clampedX, clampedY, transform.localPosition.z);
    }
}
